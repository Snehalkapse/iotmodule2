# SENSORS AND ACTUATORS

## Sensors

- a sensor is a device, module, machine, or subsystem whose purpose is to detect events or changes in its environment and send the information to other electronics devices therefore sensor is called as transducer that is used for physical energy into elctrial signal

![](assignments/pictures/Sensor___Actuators.jpg)


- The specific input could be light, heat, motion, moisture, pressure, or any one of a great number of other environmental phenomena

- For example : In a mercury-based glass thermometer, the input is temperature. The liquid contained expands and contracts in response, causing the level to be higher or lower on the marked gauge, which is human-readable. 

![](assignments/pictures/Basic-Sensors.jpg)

## Actuators

- Actuator is inversely propostional to sensor

![](assignments/pictures/intro-img.jpg )

- An actuator is a component of a machine that is responsible for moving and controlling a mechanism or system
- Some eg. of actuators are as shown in below

![](assignments/pictures/actuators.png )



# ANALOG AND DIGITAL

- Digital as well as Analog System, both are used to transmit signals from one place to another like audio/video. Digital system uses binary format as 0 and 1 whereas analog system uses electronic pulses with varing magnitude to send data

![](assignments/pictures/Analog-Signal-Vs-digital-Signal.jpg)

## Difference between Analog and digital System

| Sr. No. | Key | Digital System | Analog System | 
| ------ | ------ | ------ | ------ |
| 1 | Signal Type | Digital System uses discrete signals as on/off representing binary format. Off is 0, On is 1. | Analog System uses continous signals with varying magnitude. |
| 2 | Wave Type | Digital System uses square waves. | Analog system uses sine waves. |
| 3 | Technology | Digital system first transform the analog waves to limited set of numbers and then record them as digital square waves. | Analog systems records the physical waveforms as they are originally generated. |
| 4 | Transmission | Digital transmission is easy and can be made noise proof with no loss at all. | Analog systems are affected badly by noise during transmission. |
| 5 | Flexibility | Digital system hardware can be easily modulated as per the requirements. | Analog system's hardwares are not flexible. |
| 6 | Bandwidth | Digital transmission needs more bandwidth to carry same information. | Analog tranmission requires less bandwidth. |
| 7 | Memory | Digital data is stored in form of bits. | Analog data is stored in form of waveform signals. |
| 8 | Example | Digital system are: Computer, CD, DVD. | Analog systems are: Analog electronics, voice radio using AM frequency. |


![](assignments/pictures/analog_digital.jpg)


# MICROPROCESSOR AND MICROCONTROLLER

## MICROPROCESSOR

- A microprocessor is a computer processor that incorporates the functions of a central processing unit on a single incorporates
- The operation of microprocessor is Like other central processing units, microprocessors use three steps commonly called Fetch, Decode, and Execute.

![](assignments/pictures/diff_mc_mp.jpg )




